# Install required dependencies
apt update && apt install build-essentials

# Download the latest "stable" version of nginx
# from: https://nginx.org/en/download.html (make sure to download the tar.gz with sources)
# extract it
tar xvf nginx...tar.gz

# Checkout rtmp module
git clone https://github.com/adamk33n3r/nginx-rtmp-module.git nginx-rtmp-module

# There is a changed required to one of the source files in order to get rid of all compiler warnings.
# diff --git a/ngx_rtmp_eval.c b/ngx_rtmp_eval.c
# index 1e5195a..d446b76 100644
# --- a/ngx_rtmp_eval.c
# +++ b/ngx_rtmp_eval.c
# @@ -166,7 +166,7 @@ ngx_rtmp_eval(void *ctx, ngx_str_t *in, ngx_rtmp_eval_t **e, ngx_str_t *out,
#                          state = ESCAPE;
#                          continue;
#                  }
# -
# +            /* fall through */
#              case ESCAPE:
#                  ngx_rtmp_eval_append(&b, &c, 1, log);
#                  state = NORMAL;

# Switch into the directory where you extracted nginx
cd nginx_xxx

./configure --add-module=/root/working/nginx-rtmp-module --conf-path=/etc/nginx/nginx.conf --with-threads --with-file-aio --with-http_ssl_module --with-debug

# When the configure succeeds, execute:
make && make install

# Now nginx should be installed. You may want to create a service file.
# This will start the server automatically on boot.